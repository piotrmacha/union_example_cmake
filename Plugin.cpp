#include <Union/String.h>
#include <Union/Locale.h>
#include <Union/Hook.h>
#include <ZenGin/zGothicAPI.h>

namespace Gothic_II_Addon {

    int Hook_Options();

    auto hook2 = CreateHook(reinterpret_cast<void*>(0x0042C450), &Hook_Options, ::Union::HookType::Hook_Detours);

    int Hook_Options()
    {
        auto langName = ::Union::Locale::GetUserLocale().LanguageName;
        ::Union::StringANSI::Format("Hello World: %t", langName).ShowMessage();
        return hook2();
    }
}

EXTERN_C_START
__declspec(dllexport) void Game_Entry() {
    ::Union::StringANSI("Hello World").ShowMessage();
}
EXTERN_C_END